USE [master]
GO
/****** Object:  Database [PRN211]    Script Date: 11/1/2023 11:23:02 PM ******/
CREATE DATABASE [PRN211]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PRN211', FILENAME = N'D:\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\PRN211.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PRN211_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\PRN211_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [PRN211] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PRN211].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PRN211] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PRN211] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PRN211] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PRN211] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PRN211] SET ARITHABORT OFF 
GO
ALTER DATABASE [PRN211] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [PRN211] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PRN211] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PRN211] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PRN211] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PRN211] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PRN211] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PRN211] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PRN211] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PRN211] SET  ENABLE_BROKER 
GO
ALTER DATABASE [PRN211] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PRN211] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PRN211] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PRN211] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PRN211] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PRN211] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PRN211] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PRN211] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PRN211] SET  MULTI_USER 
GO
ALTER DATABASE [PRN211] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PRN211] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PRN211] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PRN211] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PRN211] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PRN211] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PRN211] SET QUERY_STORE = ON
GO
ALTER DATABASE [PRN211] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [PRN211]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/1/2023 11:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 11/1/2023 11:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[order_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[ordered_date] [date] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Detail]    Script Date: 11/1/2023 11:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Detail](
	[order_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[price] [money] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[order_id] ASC,
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 11/1/2023 11:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[product_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[quantity] [int] NOT NULL,
	[price] [money] NOT NULL,
	[img] [varchar](50) NOT NULL,
	[status] [int] NOT NULL,
	[desc] [varchar](250) NOT NULL,
	[category_id] [int] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/1/2023 11:23:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[address] [varchar](50) NOT NULL,
	[role] [int] NOT NULL,
	[status] [int] NOT NULL,
	[phone] [varchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([category_id], [name]) VALUES (1, N'Fiction')
INSERT [dbo].[Category] ([category_id], [name]) VALUES (2, N'Non-Fiction')
INSERT [dbo].[Category] ([category_id], [name]) VALUES (3, N'Novel')
INSERT [dbo].[Category] ([category_id], [name]) VALUES (4, N'Comic')
INSERT [dbo].[Category] ([category_id], [name]) VALUES (5, N'Travel')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (1, N'Sprider Man - Venom', 18, 20.0000, N'images/comic.jpg', 1, N'desc', 4)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (2, N'Earth''s Mightest Heroes', 20, 30.0000, N'images/comic2.jpg', 1, N'desc', 4)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (3, N'The GhostWriter', 22, 25.0000, N'images/comic3.jpg', 1, N'desc', 4)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (4, N'Superman', 12, 23.0000, N'images/comic4.jpg', 1, N'desc ', 4)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (5, N'The Girl in His Shadow', 14, 18.0000, N'images/finction1.jpg', 1, N'desc', 1)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (6, N'The Rose Code', 11, 19.0000, N'images/finction2.jpg', 1, N'desc', 1)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (7, N'A Desolation Called Peace', 23, 31.0000, N'images/finction3.jpg', 1, N'desc', 1)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (8, N'The Calculateing Stars', 40, 50.0000, N'images/finction4.jpeg', 1, N'desc', 1)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (9, N'The NonFiction Book', 50, 40.0000, N'images/nonfinction1.jpg', 1, N'desc', 2)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (10, N'I Know why the Caged Bird Sings', 60, 60.0000, N'images/nonfinction2.jpg', 1, N'desc', 2)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (11, N'Tough Guy', 70, 70.0000, N'images/nonfinction3.jpg', 1, N'desc', 2)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (12, N'Anne Frank', 80, 80.0000, N'images/nonfinction4.jpg', 1, N'desc', 2)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (13, N'The Light Private', 90, 90.0000, N'images/novel1.jpg', 1, N'desc', 3)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (14, N'Death comes to call', 100, 100.0000, N'images/novel2.jpg', 1, N'desc', 3)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (15, N'Our Missing Hearts', 110, 110.0000, N'images/novel3.jpg', 1, N'desc', 3)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (16, N'Travel Book', 120, 120.0000, N'images/travel1.jpg', 1, N'desc', 5)
INSERT [dbo].[Product] ([product_id], [name], [quantity], [price], [img], [status], [desc], [category_id]) VALUES (17, N'A Woman''s Journey through India', 130, 130.0000, N'images/travel2.jpg', 1, N'desc', 5)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (1, N'Hieu', N'hieu@gmail.com', N'12345678', N'Piltover', 1, 1, N'0866513607')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (2, N'Thinh', N'thinh@gmail.com', N'12345678', N'Ionia', 1, 1, N'0123456678')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (4, N'Duc', N'duc@gmail.com', N'12345678', N'Demacia', 1, 1, N'0123456778')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (5, N'Minh', N'minh@gmail.com', N'12345678', N'Zaun', 1, 1, N'0123345678')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (6, N'Long', N'long@gmail.com', N'12345678', N'Noxus', 1, 1, N'0123455678')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (7, N'User', N'user@gmail.com', N'12345678', N'Void', 1, 1, N'0123456789')
INSERT [dbo].[User] ([user_id], [name], [email], [password], [address], [role], [status], [phone]) VALUES (8, N'Admin', N'admin@gmail.com', N'12345678', N'Targon', 2, 1, N'0123456788')
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Order_Detail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([order_id])
REFERENCES [dbo].[Order] ([order_id])
GO
ALTER TABLE [dbo].[Order_Detail] CHECK CONSTRAINT [FK_OrderDetail_Order]
GO
ALTER TABLE [dbo].[Order_Detail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Product] ([product_id])
GO
ALTER TABLE [dbo].[Order_Detail] CHECK CONSTRAINT [FK_OrderDetail_Product]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[Category] ([category_id])
GO
USE [master]
GO
ALTER DATABASE [PRN211] SET  READ_WRITE 
GO
