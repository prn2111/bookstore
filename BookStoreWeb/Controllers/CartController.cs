﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BookStoreWeb.Controllers
{
    public class CartController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add(int productID)
        {
            using Prn211Context context = new Prn211Context();
            List<Cart>? carts = new List<Cart>();
            string? cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                carts = JsonConvert.DeserializeObject<List<Cart>>(cartString);
            }
            bool found = false;
            foreach (Cart cart in carts)
            {
                if (cart.Product.ProductId == productID) { cart.Quantity++; found = true; }
            }
            if (!found)
            {

                Product product = context.Products.SingleOrDefault(pro => pro.ProductId == productID);
                product.Category = context.Categories.Find(product.CategoryId);
                carts.Add(new Cart
                {
                    Product = product,
                    Quantity = 1
                });
            }
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(carts, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult ChangeQuantity(int id, int quantity)
        {
            List<Cart>? carts = new List<Cart>();
            string? cartString = HttpContext.Session.GetString("cart");
            if (cartString != null)
            {
                carts = JsonConvert.DeserializeObject<List<Cart>>(cartString);
            }
            foreach (Cart cart in carts)
            {
                if (cart.Product.ProductId == id)
                {
                    if (quantity == 0)
                    {
                        carts.Remove(cart);
                    }
                    else cart.Quantity = quantity;
                }
            }
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(carts, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
            return Json("Change quantity successfully");
        }
    }
}
