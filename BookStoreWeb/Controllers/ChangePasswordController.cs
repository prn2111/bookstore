﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BookStoreWeb.Controllers
{
	public class ChangePasswordController : Controller
	{
		// GET: ChangePasswordController
		public ActionResult Index()
		{

			return View();
		}

		// GET: ChangePasswordController/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: ChangePasswordController/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: ChangePasswordController/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ChangePasswordController/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: ChangePasswordController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(String oldPass, String newPass, String renewPass)
		{
			User? curUser = new User();
			string? sesUser = HttpContext.Session.GetString("user");
			if (sesUser != null)
			{
				curUser = JsonConvert.DeserializeObject<User>(sesUser);
			}
			using Prn211Context context = new Prn211Context();
			if(newPass != renewPass)
			{
				TempData["message"] = "The new password and the confirm new password are not the same!";
				return RedirectToAction("Index");
			} else if (oldPass.Equals(curUser.Password))
			{
				curUser.Password = newPass;
				context.Users.Update(curUser);
				context.SaveChanges();
				HttpContext.Session.SetString("user", JsonConvert.SerializeObject(curUser));
				TempData["message"] = "Change password successfully!";
				return RedirectToAction("Index");
			} else
			{
				TempData["message"] = "The old password is incorrect!";
				return RedirectToAction("Index");
			}
		}

		// GET: ChangePasswordController/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: ChangePasswordController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
