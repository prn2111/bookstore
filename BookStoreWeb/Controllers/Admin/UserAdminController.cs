﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.Metrics;

namespace BookStoreWeb.Controllers.Admin
{
    public class UserAdminController : Controller
    {
        public IActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"];
            }
            List<User> users = new List<User>();
            try
            {
                using var context = new Prn211Context();
                users = context.Users.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return View(users);
        }

        public IActionResult Edit(int userId)
        {
            User user = GetUserByID(userId);
            if (user == null) return NotFound();
            ViewBag.Status = user.Status;
            ViewBag.Role = user.Role;
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using var context = new Prn211Context();
                    context.Users.Update(user);
                    context.SaveChanges();
                    TempData["message"] = "Edit successfully";
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        // GET: Member/Delete/5
        public IActionResult Delete(int? userId)
        {
            User user = GetUserByID(userId.Value);
            if (user == null) return NotFound();
            return View(user);
        }

        // POST: Member/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int userId)
        {
            try
            {
                User user = GetUserByID(userId);
                if (user != null)
                {
                    using var context = new Prn211Context();
                    context.Users.Remove(user);
                    context.SaveChanges();
                    TempData["message"] = "Delete successfully";
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        public User GetUserByID(int id)
        {
            User u = null;
            try
            {
                using var context = new Prn211Context();
                u = context.Users.FirstOrDefault(pro => pro.UserId == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return u;
        }
    }
}
