﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookStoreWeb.Controllers.Admin
{
    public class OrderAdminController : Controller
    {
        public IActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.Message = TempData["message"];
            }
            List<Order> orders = new List<Order>();
            try
            {
                using var context = new Prn211Context();
                orders = context.Orders.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return View(orders);
        }

        public IActionResult Edit(int ordId)
        {
            Order ord = GetOrderByID(ordId);
            if (ord == null) return NotFound();
            ViewBag.Status = ord.Status;
            return View(ord);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Order ord)
        {
            try
            {
                using var context = new Prn211Context();
                context.Orders.Update(ord);
                context.SaveChanges();
                TempData["message"] = "Edit successfully";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        public IActionResult Detail(int ordId) 
        { 
            List<OrderDetail> orderDetails = new List<OrderDetail>();
            try
            {
                using var context = new Prn211Context();
                orderDetails = context.OrderDetails.Where(ord => ord.OrderId == ordId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return View(orderDetails);
        }

        public Order GetOrderByID(int id)
        {
            Order u = null;
            try
            {
                using var context = new Prn211Context();
                u = context.Orders.FirstOrDefault(pro => pro.OrderId == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return u;
        }
    }
}
