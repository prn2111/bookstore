﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;

namespace BookStoreWeb.Controllers.Admin
{
    public class ProductAdminController : Controller
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ProductAdminController(IWebHostEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        // GET: ProductAdminController
        public ActionResult Index(string SearchString, int ProductCategory)
        {
            using var context = new Prn211Context();
            var products = context.Products.Include(pro => pro.Category).ToList();

            if (!string.IsNullOrEmpty(SearchString))
            {
                products = products.Where(pro => pro.Name.ToLower().Contains(SearchString.ToLower())).ToList();
            }

            if (ProductCategory != 0)
            {
                products = products.Where(pro => pro.CategoryId == ProductCategory).ToList();
            }
            return View(products);
        }

        // GET: ProductAdminController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductAdminController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product pro, IFormFile file)
        {
            try
            {
                if (file != null && file.Length > 0)
                {
                    var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "images", fileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    pro.Img = "images/" + fileName;
                }

                using (var context = new Prn211Context())
                {
                    context.Products.Add(pro);
                    context.SaveChanges();
                }

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View(pro);
            }
        }

        // GET: ProductAdminController/Edit/5
        public ActionResult Edit(int proId)
        {
            Product pro = GetProductByID(proId);
            if (pro == null) return NotFound();
            ViewBag.CategoryId = pro.CategoryId;
            return View(pro);
        }

        // POST: ProductAdminController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product pro)
        {
            try
            {
                using var context = new Prn211Context();
                context.Products.Update(pro);
                context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        public Product GetProductByID(int id)
        {
            Product u = null;
            try
            {
                using var context = new Prn211Context();
                u = context.Products.Include(pro => pro.Category).FirstOrDefault(pro => pro.ProductId == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return u;
        }
    }
}
