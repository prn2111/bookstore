﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookStoreWeb.Controllers
{
	public class ProductController : Controller
	{
		// GET: ProductController
		[Route("Product/{page=1}")]
		public ActionResult Index(int page, string txtSearch = "")
		{
			using Prn211Context context = new Prn211Context();
			List<Product> products = context.Products.Where(product => product.Name.Contains(txtSearch.Trim())).ToList();
			ViewBag.CurrentPage = page;
			ViewBag.TotalPage = (products.Count() - 1) / 6 + 1;
			return View(products.Skip((page - 1) * 6).Take(6).ToList());
		}

		// GET: ProductController/Details/5
		public ActionResult Details(int id)
		{
			using Prn211Context context = new Prn211Context();
			var product = context.Products.Include(p => p.Category).FirstOrDefault(x=>x.ProductId == id);
			ViewBag.Product = product;
			var products = context.Products.ToList();
			List<Product> productsList = (from p in products
										 where p.CategoryId==product.CategoryId && p.ProductId!=product.ProductId
										 select p).Take(3).ToList();
			return View(productsList);
		}

		// GET: ProductController/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: ProductController/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ProductController/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: ProductController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: ProductController/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: ProductController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
