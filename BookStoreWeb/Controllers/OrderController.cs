﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BookStoreWeb.Controllers
{
    public class OrderController : Controller
    {
        public IActionResult Index()
        {
            using Prn211Context context = new Prn211Context();
            string? userString = HttpContext.Session.GetString("user");
            if (userString == null) return RedirectToAction("Index", "Home");
            User user = JsonConvert.DeserializeObject<User>(userString);
            List<Order> orders = context.Orders.Include(o => o.OrderDetails).Where(order => order.UserId == user.UserId).ToList();
            List<Product> products = context.Products.ToList();
            ViewBag.Products = products;
            return View(orders);
        }

        public IActionResult Details(int id)
        {
            using Prn211Context context = new Prn211Context();
            Order order = context.Orders.Find(id);
            List<OrderDetail> orderDetails = context.OrderDetails.Where(orderdetail => orderdetail.OrderId == id).ToList();
            List<Tuple<OrderDetail, Product>> detailsWithProduct = new List<Tuple<OrderDetail, Product>>();
            foreach (OrderDetail orderDetail in orderDetails)
            {
                detailsWithProduct.Add(new Tuple<OrderDetail, Product>(orderDetail, context.Products.Include(p => p.Category).SingleOrDefault(p => p.ProductId == orderDetail.ProductId)));
            }
            ViewBag.order = order;
            ViewData["detailsWithProduct"] = detailsWithProduct;
            return View();

        }

        public IActionResult Checkout()
        {
            string cartString = HttpContext.Session.GetString("cart");
            if (cartString == null)
            {
                return RedirectToAction("Index", "Product");
            }
            else
            {
                User user;
                try
                {
                    user = JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("user"));
                }
                catch (NullReferenceException ex)
                {
                    return RedirectToAction("Login", "Auth");
                }
                using Prn211Context context = new Prn211Context();
                Order order = new Order
                {
                    UserId = user.UserId,
                    OrderedDate = DateTime.Now,
                    Status = 1
                };
                context.Add(order);
                context.SaveChanges();
                int orderID = context.Orders.Max(o => o.OrderId);
                List<Cart> carts = JsonConvert.DeserializeObject<List<Cart>>(cartString);
                foreach (Cart cart in carts)
                {
                    context.Add(new OrderDetail
                    {
                        OrderId = orderID,
                        ProductId = cart.Product.ProductId,
                        Price = cart.Product.Price,
                        Quantity = cart.Quantity
                    });
                    context.SaveChanges();
                }
                HttpContext.Session.Remove("cart");
            }
            return RedirectToAction("Index");
        }

        public IActionResult ChangeStatus(int id, int status)
        {
            using Prn211Context context = new Prn211Context();
            Order order = context.Orders.Find(id);
            if ((order.Status == 1 && status == 0) || (order.Status == 2 && status == 3))
            {
                order.Status = status;
            }
            context.Entry<Order>(order).State = EntityState.Modified;
            context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
