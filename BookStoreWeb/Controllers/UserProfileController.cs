﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BookStoreWeb.Controllers
{
	public class UserProfileController : Controller
	{
		// GET: UserProfileController
		public ActionResult Index()
		{
			User? curUser = new User();
			string? user = HttpContext.Session.GetString("user");
			if (user != null)
			{
				curUser = JsonConvert.DeserializeObject<User>(user);
			}
			return View(curUser);
		}

		// GET: UserProfileController/Details/5
		public ActionResult Details(int id)
		{
			return View();
		}

		// GET: UserProfileController/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: UserProfileController/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}

		// GET: UserProfileController/Edit/5
		public ActionResult Edit(int id)
		{
			return View();
		}

		// POST: UserProfileController/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(User user)
		{
			using Prn211Context context = new Prn211Context();
			User oldUser = context.Users.SingleOrDefault(x=>x.UserId == user.UserId);
			oldUser.Name = user.Name;
			oldUser.Address = user.Address;
			oldUser.Phone = user.Phone;
			context.Users.Update(oldUser);
			context.SaveChanges();
			HttpContext.Session.SetString("user", JsonConvert.SerializeObject(oldUser));
			return RedirectToAction("Index");
		}

		// GET: UserProfileController/Delete/5
		public ActionResult Delete(int id)
		{
			return View();
		}

		// POST: UserProfileController/Delete/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int id, IFormCollection collection)
		{
			try
			{
				return RedirectToAction(nameof(Index));
			}
			catch
			{
				return View();
			}
		}
	}
}
