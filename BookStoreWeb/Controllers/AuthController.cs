﻿using BookStoreWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BookStoreWeb.Controllers
{
    public class AuthController : Controller
    {
        public IActionResult Login()
        {
            if (HttpContext.Session.GetString("user") != null) return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(User user)
        {
            using Prn211Context context = new Prn211Context();
            User? userInfo = context.Users.SingleOrDefault(u => u.Email.Equals(user.Email) && u.Password.Equals(user.Password));
            if (userInfo == null)
            {
                TempData["message"] = "The email or password is incorrect!";
                return RedirectToAction("Login");
            }
            if (userInfo.Status == 0)
            {
                TempData["message"] = "You are banned!";
                return RedirectToAction("Login");
            }
            HttpContext.Session.SetString("user", JsonConvert.SerializeObject(userInfo));
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(User user)
        {
            using Prn211Context context = new Prn211Context();
            if (HttpContext.Session.GetString("user") != null) return RedirectToAction("Index", "Home");
            if (context.Users.SingleOrDefault(u => u.Email.Equals(user.Email)) != null)
            {
                TempData["message"] = "The email already exists!";
                return RedirectToAction("Login");
            }
            user.Status = 1;
            user.Role = 1;
            context.Add(user);
            context.SaveChanges();
            TempData["message"] = "Register sucessfully!";
            return RedirectToAction("Login");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
